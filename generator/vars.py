from pathlib import Path

hosts_group: str = "peers"
inventory_groups_template_file: str = "20-groups.yml.j2"

root: Path = Path('.')
templates_path: Path = root.joinpath('generator/templates/')
inventory_folder: Path = root.joinpath('inventory/')
inventory_file: Path = inventory_folder.joinpath('10-inventory.yml')
inventory_groups: Path = inventory_folder.joinpath('20-groups.yml')
host_vars: Path = inventory_folder.joinpath('host_vars/')
group_vars: Path = inventory_folder.joinpath('group_vars/')
tunnel_vars: Path = group_vars.joinpath('tunnel.yml')
pass_file: Path = root.joinpath('vault_pass')
# files: Path = root.joinpath('files/')

start_wireguard_port: int = 51870
wireguard_ip_suffix: int = 128
start_l2tp_port: int = 61870
l2tp_ip_suffix: int = wireguard_ip_suffix
start_l2tp_id: int = 10
l2tp_mtu: int = 4310
