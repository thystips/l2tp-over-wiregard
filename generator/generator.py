import ipaddress
from itertools import combinations

import yaml
from yaml import CLoader
from ansible.inventory.manager import InventoryManager
from ansible.parsing.dataloader import DataLoader
from ansible_vault import Vault
from jinja2 import Environment, FileSystemLoader

from generator.vars import *
from generator.wireguard import generate_wireguard_shared_key


class YamlDumper(yaml.Dumper):
    def increase_indent(self, flow=False, indentless=False):
        return super(YamlDumper, self).increase_indent(flow, False)


def get_inventory(source: Path):
    loader = DataLoader()
    inventory = InventoryManager(loader=loader, sources=str(source))

    x = dict()
    x.update(inventory.groups[hosts_group].serialize()["vars"])
    group_dict = inventory.get_groups_dict()
    x.update({hosts_group: group_dict[hosts_group]})
    return x, group_dict


def get_tunnels_count(hosts: list):
    hosts_count = len(hosts)
    x = hosts_count * (hosts_count - 1) / 2
    return int(x)


def create_tunnel_config(conf, groups):
    wireguard_network_prefix = ipaddress.ip_network(conf["wireguard_network_prefix"])[0]
    l2tp_network_prefix = ipaddress.ip_network(conf["l2tp_network_prefix"])[0]
    wireguard_port = start_wireguard_port
    l2tp_port = start_l2tp_port
    l2tp_session = start_l2tp_id
    wireguard_interface_number = 0
    hosts_c = dict()
    tun_list = []
    hosts_tuns = {}

    for combination in combinations(groups[hosts_group], 2):
        _wireguard_interface = "wg" + str(wireguard_interface_number)
        wireguard_network_prefix += 2
        l2tp_network_prefix += 3
        l2tp_session += 2
        l2tp_port += 2
        name = "tun" + str(wireguard_interface_number)

        hosts_c.update(
            {
                name: {
                    "tunnel_name": name,
                    "l2tp_port": l2tp_port,
                    "l2tp_mtu": l2tp_mtu,
                    "wg_port": wireguard_port,
                    "wg_int": _wireguard_interface,
                    "peers": [
                        {
                            "host": str(combination[0]),
                            "peer": str(combination[1]),
                            "tunnel_name": name,
                            "l2tp_port": l2tp_port - 1,
                            "l2tp_peer_port": l2tp_port,
                            "l2tp_mtu": l2tp_mtu,
                            "wg_port": wireguard_port,
                            "wg_int": _wireguard_interface,
                            "ip_suffix": wireguard_ip_suffix,
                            "ip": str(wireguard_network_prefix - 1),
                            "peer_ip": str(wireguard_network_prefix),
                            "l2tp_ip": str(l2tp_network_prefix - 2),
                            "l2tp_peer_ip": str(l2tp_network_prefix - 1),
                            "l2tp_ip_suffix": wireguard_ip_suffix,
                            "id": l2tp_session - 1,
                            "peer_id": l2tp_session,
                        },
                        {
                            "host": str(combination[1]),
                            "peer": str(combination[0]),
                            "tunnel_name": name,
                            "l2tp_port": l2tp_port,
                            "l2tp_peer_port": l2tp_port - 1,
                            "l2tp_mtu": l2tp_mtu,
                            "wg_port": wireguard_port,
                            "wg_int": _wireguard_interface,
                            "ip_suffix": wireguard_ip_suffix,
                            "ip": str(wireguard_network_prefix),
                            "peer_ip": str(wireguard_network_prefix - 1),
                            "l2tp_ip": str(l2tp_network_prefix - 1),
                            "l2tp_peer_ip": str(l2tp_network_prefix - 2),
                            "l2tp_ip_suffix": wireguard_ip_suffix,
                            "id": l2tp_session,
                            "peer_id": l2tp_session - 1,
                        },
                    ],
                }
            }
        )

        wireguard_interface_number += 1
        wireguard_port += 1

        tun_list.append(name)
        for host in combination:
            if host in hosts_tuns:
                hosts_tuns[host].append(name)
            else:
                hosts_tuns.update({host: [name]})
    return hosts_c, wireguard_network_prefix, tun_list, hosts_tuns


def create_group_vars_file(conf: dict):
    for group in conf:
        group = group.lower()
        _var = group_vars.joinpath(group + ".yml")
        _data: dict = {conf[group]["tunnel_name"]: conf[group]}
        _var.touch(exist_ok=True)
        with open(_var, "w") as outfile:
            yaml.dump(_data, outfile, Dumper=YamlDumper, default_flow_style=False)


def create_groups_inventory(conf: dict):
    _environment = Environment(loader=FileSystemLoader(templates_path))
    _template = _environment.get_template(inventory_groups_template_file)
    _content = _template.render(data=conf)
    with open(inventory_groups, mode="w", encoding="utf-8") as file:
        file.write(_content)


def load_host_vars(file):
    if file.exists():
        with open(file, "r") as var_file:
            data = yaml.load(var_file, Loader=CLoader)
    else:
        data = {}
    return data


def write_host_vars(file, data):
    with open(file, "w") as var_file:
        yaml.dump(data, var_file, Dumper=YamlDumper, default_flow_style=False)


def create_host_vars_tunnels(hosts_tuns, path, host):
    data = load_host_vars(path)
    if data is None:
        data = {"tunnels": hosts_tuns.get(host)}
    elif "tunnels" in data:
        data["tunnels"] = hosts_tuns.get(host)
    else:
        data.update({"tunnels": hosts_tuns.get(host)})
    write_host_vars(path, data)


def create_tunnel_shared_keys(password, tun_list):
    _structure = {"tunnels_key": {}}
    v = {}
    vault = Vault(password)

    if tunnel_vars.exists():
        v = vault.load(open(tunnel_vars).read())
        tunnels_key = v["tunnels_key"]
    else:
        tunnel_vars.touch()
        v.update(_structure)
        tunnels_key = {}

    for tun in tun_list:
        if not tun in tunnels_key:
            key = generate_wireguard_shared_key()
            tunnels_key.update({tun: key})

    v["tunnels_key"] = tunnels_key
    vault.dump(v, open(tunnel_vars, "w"))


def create_host_vars_router_id(conf, hosts_list):
    key = "router_id"
    key_v6 = "router_id_v6"
    key_cluster = "cluster_id"
    router_id = ipaddress.ip_network(conf["router_id_network"])[0]
    router_id_v6 = ipaddress.ip_network(conf["router_id_network_v6"])[0]
    for h in hosts_list:
        router_id += 1
        router_id_v6 += 1
        _path = host_vars.joinpath(h + ".yml")
        data = load_host_vars(_path)
        if data is None:
            if str(h).startswith("RR"):
                data = {
                    key: str(router_id),
                    key_v6: str(router_id_v6),
                    key_cluster: str(router_id),
                }
            else:
                data = {key: str(router_id), key_v6: str(router_id_v6)}
        elif key in data:
            data[key] = str(router_id)
            data[key_v6] = str(router_id_v6)
            if str(h).startswith("RR"):
                data[key_cluster] = str(router_id)
        else:
            if str(h).startswith("RR"):
                data.update(
                    {
                        key: str(router_id),
                        key_v6: str(router_id_v6),
                        key_cluster: str(router_id),
                    }
                )
            else:
                data.update({key: router_id, key_v6: router_id_v6})
        write_host_vars(_path, data)
