import subprocess


def generate_wireguard_keys():
    """
    Generate a WireGuard private & public key
    Requires that the 'wg' command is available on PATH
    Returns private_key and public_key as strings
    """
    privkey = subprocess.check_output("wg genkey", shell=True).decode("utf-8").strip()
    pubkey = subprocess.check_output(f"echo '{privkey}' | wg pubkey", shell=True).decode("utf-8").strip()
    return privkey, pubkey


def generate_wireguard_shared_key():
    """
    Generate a WireGuard shared key
    Requires that the 'wg' command is available on PATH
    Returns shared_key as strings
    """
    sharedkey = subprocess.check_output("wg genpsk", shell=True).decode("utf-8").strip()
    return sharedkey
