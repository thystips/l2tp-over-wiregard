#!/usr/bin/python

from generator import *

if __name__ == "__main__":
    config, groups = get_inventory(inventory_file.resolve(True))

    print("Number of tunnels:", get_tunnels_count(groups[hosts_group]))

    tunnel_config, wg_network_prefix, tunnels_list, hosts_tunnels = create_tunnel_config(config, groups)

    create_group_vars_file(tunnel_config)
    create_groups_inventory(tunnel_config)
    
    for h in hosts_tunnels:
        path = host_vars.joinpath(h + '.yml')
        create_host_vars_tunnels(hosts_tunnels, path, h)

    create_host_vars_router_id(config, hosts_tunnels)

    password = open(pass_file, "r").read()
    create_tunnel_shared_keys(password, tunnels_list)
